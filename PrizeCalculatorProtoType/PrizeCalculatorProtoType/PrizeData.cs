﻿namespace PrizeCalculatorProtoType
{
    public class PrizeData
    {
        public PrizeData(
            int collectedMoney, 
            decimal rakePercentage, 
            int prizePlacements)
        {
            CollectedMoney = collectedMoney;
            RakePercentage = rakePercentage;
            PrizePlacements = prizePlacements;
        }

        public int CollectedMoney { get; }
        public decimal RakePercentage { get; }
        public int PrizePlacements { get; }
    }
}
