﻿using System.Text.Json.Serialization;

namespace PrizeCalculatorProtoType
{
    public class Payout
    {
        public Payout(
            decimal firstPlace = 0, 
            decimal secondPlace = 0, 
            decimal thirdPlace = 0, 
            decimal fourthPlace = 0, 
            decimal fifthPlace = 0, 
            decimal sixthPlace = 0)
        {
            FirstPlace = firstPlace;
            SecondPlace = secondPlace;
            ThirdPlace = thirdPlace;
            FourthPlace = fourthPlace;
            FifthPlace = fifthPlace;
            SixthPlace = sixthPlace;
        }

        public decimal FirstPlace { get; } = 0;
        public decimal SecondPlace { get; } = 0;
        public decimal ThirdPlace { get; } = 0;
        public decimal FourthPlace { get; } = 0;
        public decimal FifthPlace { get; } = 0;
        public decimal SixthPlace { get; } = 0;

        public decimal GetPlaceByIndex(int index)
        {
            switch (index)
            {
                case 0: return FirstPlace;
                case 1: return SecondPlace;
                case 2: return ThirdPlace;
                case 3: return FourthPlace;
                case 4: return FifthPlace;
                case 5: return SixthPlace;
            }

            throw new IndexOutOfRangeException();
        }
    }
}
