﻿using Newtonsoft.Json;

namespace PrizeCalculatorProtoType
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var mainMenu = new MainMenu();

            SeedPayouts();

            mainMenu.Run();
        }

        static void SeedPayouts()
        {
            var twoPlaces = JsonConvert.SerializeObject(new Payout(0.60m, 0.40m));
            var threePlaces = JsonConvert.SerializeObject(new Payout(0.50m, 0.30m, 0.20m));
            var fourPlaces = JsonConvert.SerializeObject(new Payout(0.40m, 0.27m, 0.19m, 0.14m));
            var fivePlaces = JsonConvert.SerializeObject(new Payout(0.36m, 0.25m, 0.18m, 0.12m, 0.09m));
            var sixPlaces = JsonConvert.SerializeObject(new Payout(0.34m, 0.23m, 0.16m, 0.12m, 0.08m, 0.07m));

            File.WriteAllText("twoPlaces", twoPlaces);
            File.WriteAllText("threePlaces", threePlaces);
            File.WriteAllText("fourPlaces", fourPlaces);
            File.WriteAllText("fivePlaces", fivePlaces);
            File.WriteAllText("sixPlaces", sixPlaces);
        }
    }
}