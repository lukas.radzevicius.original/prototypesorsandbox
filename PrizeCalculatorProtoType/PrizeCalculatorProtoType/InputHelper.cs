﻿using CodeHelper;

namespace PrizeCalculatorProtoType
{
    public class InputHelper
    {
        public static int GetCollectedMoney()
        {
            return ConsoleHelper.ReadInt("Įveskite kiek surinkta pinigų: ");
        }

        public static decimal GetRakePercentage() 
        {
            return ConsoleHelper.ReadDecimal("Įveskite aptarnavimo mokestį procentais: ");
        }

        public static int GetPrizePlacements() 
        {
            return ConsoleHelper.ReadInt("Iveskite kiek bus prizinių vietų: ");
        }
    }
}
