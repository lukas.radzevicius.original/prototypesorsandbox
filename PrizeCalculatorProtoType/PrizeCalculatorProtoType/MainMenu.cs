﻿using Newtonsoft.Json;

namespace PrizeCalculatorProtoType
{
    public class MainMenu
    {
        const string PathToFile = "prizeData.json";

        public void Run()
        {
            Console.WriteLine("Prizu skaiciuokle:");

            do
            {
                Console.Clear();

                Console.WriteLine("1. Įvesti duomenis");
                Console.WriteLine("2. Išvesti prizines vietas");
                Console.WriteLine("q. Išeiti");

                string userInput = Console.ReadLine();

                if (userInput.ToLower() == "q")
                    break;

                switch (userInput)
                {
                    case "1":
                        GetUserInputs();
                        break;
                    case "2":
                        CalculateAndPlaceToDictionary();
                        break;
                }
            } while (true);
        }

        private void GetUserInputs()
        {
            Console.Clear();

            var collectedMoney = InputHelper.GetCollectedMoney();
            var prizePlacements = InputHelper.GetPrizePlacements();
            var rakePercentage = InputHelper.GetRakePercentage();

            var prizeData = new PrizeData(collectedMoney, rakePercentage, prizePlacements);

            SavePrizeDataAsJson(prizeData);
        }

        private void SavePrizeDataAsJson(PrizeData data)
        {
            var json = JsonConvert.SerializeObject(data);

            File.WriteAllText(PathToFile, json);
        }

        private void CalculateAndPlaceToDictionary()
        {
            Console.Clear();

            var json = File.ReadAllText(PathToFile);

            var prizeData = JsonConvert.DeserializeObject<PrizeData>(json);
            var rake = prizeData.CollectedMoney * (prizeData.RakePercentage / 100);
            var prizePool = prizeData.CollectedMoney - rake;

            Payout payout;

            try
            {
                payout = GetPayout(prizeData.PrizePlacements);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            var totalMoneyPaid = 0m;
            var moneyLeftToPay = 0m;

            // place all the prizes to a dictionary, for easy adjustment of prizes later.
            var placementsAndPrizes = new Dictionary<int, decimal>();

            for (int i = 0; i < prizeData.PrizePlacements; i++)
            {
                var displayPrize = Math.Round(prizePool * payout.GetPlaceByIndex(i), 0);

                while (displayPrize % 5 != 0)
                {
                    displayPrize--;
                }

                if (displayPrize % 5 == 0)
                {
                    placementsAndPrizes.Add(i + 1, displayPrize);
                    totalMoneyPaid += displayPrize;
                }

                if (i == prizeData.PrizePlacements - 1)
                {
                    moneyLeftToPay = prizePool - totalMoneyPaid;

                    placementsAndPrizes.Add(i + 2, 0m);
                }
            }

            if (moneyLeftToPay < 20)
                placementsAndPrizes[1] += Math.Round(moneyLeftToPay, 0);

            foreach (var key in placementsAndPrizes.Keys)
            {
                if (!(placementsAndPrizes[key] == 0))
                    Console.WriteLine($"{key}. {placementsAndPrizes[key]}");
                else
                    Console.WriteLine($"{key}. FREEPASS");
            }

            Console.WriteLine();
            Console.WriteLine($"Viso surinkta: {prizeData.CollectedMoney}");
            Console.WriteLine($"Aptarnavimo mokestis: {rake}");
            Console.WriteLine($"Išmokėta: {placementsAndPrizes.Values.Sum()}");
            Console.WriteLine($"Liko išmokėti: {prizePool - placementsAndPrizes.Values.Sum()}");
            Console.WriteLine();
            Console.WriteLine("Spauskite bet kokį mygtuką, kad grįšti į Menu...");
            Console.ReadKey();
            Console.WriteLine();
        }

        private Payout? GetPayout(int prizePlacements)
        {
            switch (prizePlacements)
            {
                case 2:
                    return JsonConvert.DeserializeObject<Payout>(File.ReadAllText("twoPlaces"));
                case 3:
                    return JsonConvert.DeserializeObject<Payout>(File.ReadAllText("threePlaces"));
                case 4:
                    return JsonConvert.DeserializeObject<Payout>(File.ReadAllText("fourPlaces"));
                case 5:
                    return JsonConvert.DeserializeObject<Payout>(File.ReadAllText("fivePlaces"));
                case 6:
                    return JsonConvert.DeserializeObject<Payout>(File.ReadAllText("sixPlaces"));
            }

            throw new InvalidDataException("PrizePlacements irasytas maziau nei 1 arba daugiau nei 6");
        }
    }
}
